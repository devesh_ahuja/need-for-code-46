@extends('layouts.app')

@section('content')

<div class="center shadow p-3 mb-5 bg-body rounded">
    <h1>Sign-up</h1>
    <form method="POST" action="{{ route('register') }}">
        @csrf

        
        <div class="txt_field">
            <input id="name" type="text" class=" @error('name') is-invalid @enderror" name="name"
            value="{{ old('name') }}" required autocomplete="name" autofocus>
            <label for="name" >{{ __('Name') }}</label>

                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

        
            
            <div class="txt_field">
                <input id="email" type="email" class=" @error('email') is-invalid @enderror" name="email"
                value="{{ old('email') }}" required autocomplete="email">
                
                <label for="email" >{{ __('Email Address') }}</label>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

        
            
            <div class="txt_field">
                <input id="password" type="password" class=" @error('password') is-invalid @enderror"
                name="password" required autocomplete="new-password">
                
                <label for="password" >{{ __('Password') }}</label>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

        <div class="txt_field">
            <input id="password-confirm" type="password" name="password_confirmation" required
            autocomplete="new-password">
            
                <label for="password-confirm"
                    >{{ __('Confirm Password') }}</label>
        </div>

        
            <div>
                <button type="submit">
                    {{ __('Register') }}
                </button>
            </div>
    </form>
</div>
@endsection
