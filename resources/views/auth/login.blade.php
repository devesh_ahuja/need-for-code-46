@extends('layouts.app')

@section('content')
<div class="center shadow p-3 mb-5 bg-body rounded">
    <h1>Login</h1>
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="txt_field">
            <input id="email" type="email" class="@error('email') is-invalid @enderror" name="email"
                value="{{ old('email') }}" required autocomplete="email" autofocus>
            <label for="email">{{ __('Email Address') }}</label>
        </div>
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

        <div class="txt_field">
            <input id="password" type="password" class="@error('password') is-invalid @enderror" name="password"
                required autocomplete="current-password">
            <label for="password">{{ __('Password') }}</label>
        </div>
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror



        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="remember" id="remember"
                {{ old('remember') ? 'checked' : '' }}>

            <label class="mb-md-3" for="remember">
                {{ __('Remember Me') }}
            </label>
        </div>




        
        <div class="pass">Forgot Password?</div>
        <button type="submit">
            {{ __('Login') }}
        </button>
        @if (Route::has('password.request'))
        <a href="{{ route('password.request') }}">
            </a>
        @endif

            <div class="signup_link">
                Not a member? <a href="#">Signup</a>
            </div>

    </form>

    @endsection
